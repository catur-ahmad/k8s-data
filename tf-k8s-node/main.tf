provider "libvirt" {
    uri = "qemu:///system"
}

terraform {
 required_version = ">= 0.13"
  required_providers {
    libvirt = {
      source  = "dmacvicar/libvirt"
      version = "0.6.2"
    }
  }
}

resource "libvirt_cloudinit_disk" "ca-master1-cloudinit" {
    name = "ca-master1-cloudinit.iso"
    pool = "vms-root"
    user_data = data.template_file.user1_data.rendered
    network_config = data.template_file.network1_config.rendered
}

data "template_file" "user1_data" {
    template = file("${path.module}/cloudinit1.cfg")
}

data "template_file" "network1_config" {
    template = file("${path.module}/network1_config.cfg")
}

resource "libvirt_volume" "ca-master1-vda" {
    name = "ca-master1-vda.qcow2"
    pool = "vms-root"
    base_volume_name = "jammy-server-cloudimg-amd64.img"
    base_volume_pool = "isos"
    size = "53687091200"
    format = "qcow2"
}

resource "libvirt_domain" "ca-master1" {
    name = "ca-master1"
    memory = "4096"
    vcpu = "4"
    machine = ""

    cpu = {
           mode = "host-passthrough"
    }

    cloudinit = libvirt_cloudinit_disk.ca-master1-cloudinit.id

    console {
        type        = "pty"
        target_port = "0"
        target_type = "serial"
    }

    console {
        type        = "pty"
        target_port = "1"
        target_type = "virtio"
    }

    network_interface {
        network_name = "ca-10.50.0"
        addresses = ["10.50.0.201"]
    }

    network_interface {
        network_name = "ca-10.50.60"
        addresses = ["10.50.60.201"]
    }

    disk {
        volume_id = libvirt_volume.ca-master1-vda.id
    }

    video {
        type = "vga"
    }

    graphics {
        type = "spice"
        listen_type = "address"
        autoport = true
    }
}

resource "libvirt_cloudinit_disk" "ca-worker1-cloudinit" {
    name = "ca-worker1-cloudinit.iso"
    pool = "vms-root"
    user_data = data.template_file.user2_data.rendered
    network_config = data.template_file.network2_config.rendered
}

data "template_file" "user2_data" {
    template = file("${path.module}/cloudinit2.cfg")
}

data "template_file" "network2_config" {
    template = file("${path.module}/network2_config.cfg")
}

resource "libvirt_volume" "ca-worker1-vda" {
    name = "ca-worker1-vda.qcow2"
    pool = "vms-root"
    base_volume_name = "jammy-server-cloudimg-amd64.img"
    base_volume_pool = "isos"
    size = "53687091200"
    format = "qcow2"
}

resource "libvirt_domain" "ca-worker1" {
    name = "ca-worker1"
    memory = "4096"
    vcpu = "4"
    machine = ""

    cpu = {
           mode = "host-passthrough"
    }

    cloudinit = libvirt_cloudinit_disk.ca-worker1-cloudinit.id

    console {
        type        = "pty"
        target_port = "0"
        target_type = "serial"
    }

    console {
        type        = "pty"
        target_port = "1"
        target_type = "virtio"
    }

    network_interface {
        network_name = "ca-10.50.0"
        addresses = ["10.50.0.202"]
    }

    network_interface {
        network_name = "ca-10.50.60"
        addresses = ["10.50.60.202"]
    }

    disk {
        volume_id = libvirt_volume.ca-worker1-vda.id
    }

    video {
        type = "vga"
    }

    graphics {
        type = "spice"
        listen_type = "address"
        autoport = true
    }
}

resource "libvirt_cloudinit_disk" "ca-worker2-cloudinit" {
    name = "ca-worker2-cloudinit.iso"
    pool = "vms-root"
    user_data = data.template_file.user3_data.rendered
    network_config = data.template_file.network3_config.rendered
}

data "template_file" "user3_data" {
    template = file("${path.module}/cloudinit3.cfg")
}

data "template_file" "network3_config" {
    template = file("${path.module}/network3_config.cfg")
}

resource "libvirt_volume" "ca-worker2-vda" {
    name = "ca-worker2-vda.qcow2"
    pool = "vms-root"
    base_volume_name = "jammy-server-cloudimg-amd64.img"
    base_volume_pool = "isos"
    size = "53687091200"
    format = "qcow2"
}

resource "libvirt_domain" "ca-worker2" {
    name = "ca-worker2"
    memory = "4096"
    vcpu = "4"
    machine = ""

    cpu = {
           mode = "host-passthrough"
    }

    cloudinit = libvirt_cloudinit_disk.ca-worker2-cloudinit.id

    console {
        type        = "pty"
        target_port = "0"
        target_type = "serial"
    }

    console {
        type        = "pty"
        target_port = "1"
        target_type = "virtio"
    }

    network_interface {
        network_name = "ca-10.50.0"
        addresses = ["10.50.0.203"]
    }

    network_interface {
        network_name = "ca-10.50.60"
        addresses = ["10.50.60.203"]
    }

    disk {
        volume_id = libvirt_volume.ca-worker2-vda.id
    }

    video {
        type = "vga"
    }

    graphics {
        type = "spice"
        listen_type = "address"
        autoport = true
    }
}
